
var datetime = new Date();
//Total seconds of the time when page is loaded
var secondsOnLoad = datetime.getTime();
secondsOnLoad = secondsOnLoad / 1000;
secondsOnLoad = Math.floor(secondsOnLoad);

//procedure performed after the block is clicked
function move()
{
    var newTop = (Math.random()) * 500;
    var newLeft = (Math.random()) * 500;
    newTop = Math.floor(newTop);
    newLeft = Math.floor(newLeft);
    var newDate = new Date();
    //Total seconds of the time when when box clicked
    var newSecondsOnClick = newDate.getTime();
    newSecondsOnClick = newSecondsOnClick / 1000;
    newSecondsOnClick = Math.floor(newSecondsOnClick);
    document.getElementById("block-displayed").setAttribute("style", "top:" + newTop + "px;left:" + newLeft + "px;");
    document.getElementById("time-elapsed").innerHTML = "Time elapsed=" + (newSecondsOnClick - secondsOnLoad) + " seconds";
    secondsOnLoad = newSecondsOnClick;
}
;
        